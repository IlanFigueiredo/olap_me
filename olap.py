
import pandas as pd
import pandasql as ps
import numpy
import datetime
import json
from matplotlib import pyplot as plt

def gen_telefonia():
    datasets_telefonia = [
        './data/acessos_telefonia_movel/Densidade_Telefonia_Movel.csv',
        './data/acessos_telefonia_movel/Acessos_Telefonia_Movel_2005-2018_Modalidade.csv',
        './data/acessos_telefonia_movel/Acessos_Telefonia_Movel_2005-2018_Tecnologia.csv',
        './data/acessos_telefonia_movel/Acessos_Telefonia_Movel_200902-2018_Modalidade_Colunas.csv',
        './data/acessos_telefonia_movel/Acessos_Telefonia_Movel_200902-2018_Tecnologia_Colunas.csv',
        ]

    a = [
        './data/acessos_telefonia_movel/Acessos_Telefonia_Movel_201901-201906_Colunas.csv',
        './data/acessos_telefonia_movel/Acessos_Telefonia_Movel_201907-201912_Colunas.csv',
        './data/acessos_telefonia_movel/Acessos_Telefonia_Movel_202001-202006_Colunas.csv',
        './data/acessos_telefonia_movel/Acessos_Telefonia_Movel_202007-202012_Colunas.csv',
        './data/acessos_telefonia_movel/Acessos_Telefonia_Movel_202101-202106_Colunas.csv',
        './data/acessos_telefonia_movel/Acessos_Telefonia_Movel_202107-202112_Colunas.csv',
        './data/acessos_telefonia_movel/Acessos_Telefonia_Movel_202201-202106_Colunas.csv',
        './data/acessos_telefonia_movel/Acessos_Telefonia_Movel_202207-202212_Colunas.csv',
        './data/acessos_telefonia_movel/Acessos_Telefonia_Movel_201901-201906.csv',
        './data/acessos_telefonia_movel/Acessos_Telefonia_Movel_201907-201912.csv',
        './data/acessos_telefonia_movel/Acessos_Telefonia_Movel_202001-202006.csv',
        './data/acessos_telefonia_movel/Acessos_Telefonia_Movel_202007-202012.csv',
        './data/acessos_telefonia_movel/Acessos_Telefonia_Movel_202101-202106.csv',
        './data/acessos_telefonia_movel/Acessos_Telefonia_Movel_202107-202112.csv',
        './data/acessos_telefonia_movel/Acessos_Telefonia_Movel_202201-202206.csv',
        './data/acessos_telefonia_movel/Acessos_Telefonia_Movel_202207-202212.csv',
        ]

    #'./data/acessos_telefonia_movel/Acessos_Telefonia_Movel_201901-201906.csv',
    n_datasets_telefonia_1 = [
        './data/acessos_telefonia_movel/Acessos_Telefonia_Movel_201907-201912.csv',
        './data/acessos_telefonia_movel/Acessos_Telefonia_Movel_202001-202006.csv',
        './data/acessos_telefonia_movel/Acessos_Telefonia_Movel_202007-202012.csv',
        './data/acessos_telefonia_movel/Acessos_Telefonia_Movel_202101-202106.csv',
        './data/acessos_telefonia_movel/Acessos_Telefonia_Movel_202107-202112.csv',
        './data/acessos_telefonia_movel/Acessos_Telefonia_Movel_202201-202206.csv',
        './data/acessos_telefonia_movel/Acessos_Telefonia_Movel_202207-202212.csv',
    ]

    datasets_telefonia_0 = './acessos_telefonia_movel_tratados/Acessos_Telefonia_Movel_201901-201906.csv',
    datasets_telefonia_1 = [
        './acessos_telefonia_movel_tratados/Acessos_Telefonia_Movel_201907-201912.csv',
        './acessos_telefonia_movel_tratados/Acessos_Telefonia_Movel_202001-202006.csv',
        './acessos_telefonia_movel_tratados/Acessos_Telefonia_Movel_202007-202012.csv',
        './acessos_telefonia_movel_tratados/Acessos_Telefonia_Movel_202101-202106.csv',
        './acessos_telefonia_movel_tratados/Acessos_Telefonia_Movel_202107-202112.csv',
        './acessos_telefonia_movel_tratados/Acessos_Telefonia_Movel_202201-202206.csv',
        './acessos_telefonia_movel_tratados/Acessos_Telefonia_Movel_202207-202212.csv',
    ]

    datasets_telefonia_2 = [
        './data/acessos_telefonia_movel/Acessos_Telefonia_Movel_201901-201906_Colunas.csv',
        './data/acessos_telefonia_movel/Acessos_Telefonia_Movel_201907-201912_Colunas.csv',
        './data/acessos_telefonia_movel/Acessos_Telefonia_Movel_202001-202006_Colunas.csv',
        './data/acessos_telefonia_movel/Acessos_Telefonia_Movel_202007-202012_Colunas.csv',
        './data/acessos_telefonia_movel/Acessos_Telefonia_Movel_202101-202106_Colunas.csv',
        './data/acessos_telefonia_movel/Acessos_Telefonia_Movel_202107-202112_Colunas.csv',
        './data/acessos_telefonia_movel/Acessos_Telefonia_Movel_202201-202106_Colunas.csv',
        './data/acessos_telefonia_movel/Acessos_Telefonia_Movel_202207-202212_Colunas.csv',
    ]

    datasets_app_magic = [
        './data/app_magic/tags.json',
        ]

    #dados_telefonia = pd.read_csv(datasets_telefonia[0], sep=';')
    #dados_telefonia.dropna(inplace=True) # Nothing dropped

    #for dataset in datasets_telefonia:
    #    set = pd.read_csv(dataset, sep=';', nrows=10)
    #    print(f"{dataset}, {set.columns}")


    dados_telefonia_1 = pd.read_csv('./acessos_telefonia_movel_tratados/Acessos_Telefonia_Movel_201901-201906.csv', sep=',')
    #query = "SELECT Ano, Mês, UF, 'Código Nacional', Tecnologia, 'Tecnologia Geração', Acessos \
    #        FROM dados_telefonia_1 "
    #dados_telefonia_1 = ps.sqldf(query)
    #print(dados_telefonia_1)

    ''''''
    for dados in datasets_telefonia_1:
        df = pd.read_csv(dados, sep=',')
        #query = "SELECT Ano, Mês, UF, 'Código Nacional', Tecnologia, 'Tecnologia Geração', Acessos \
        #    FROM df "
        #df = ps.sqldf(query)
        dados_telefonia_1 = dados_telefonia_1.append(df)

    dados_telefonia_1.to_csv("Acessos_Telefonia_Movel.csv")
    dados_telefonia_1.columns = ["Ano", "Mês", "UF", "Código_Nacional", "Tecnologia", "Tecnologia_Geração", "Tipo_de_Produto", "Acessos", "Tipo de Pessoa"]
    query = "SELECT Ano, Mês, UF, Código_Nacional, Tecnologia, Tecnologia_Geração, Acessos\
            FROM dados_telefonia_1"
    dados_telefonia_1 = ps.sqldf(query)

    print(dados_telefonia_1)
    dados_telefonia_1.to_csv("Acessos_Telefonia_Movel.csv")

    # './data/acessos_telefonia_movel/Acessos_Telefonia_Movel_202207-202212_Colunas.csv',
    '''
    SELECT Codigo Nacional, Tecnologia, Tecnologia Geração, Tipo de Produto, Acessos
    '''

    ''''
    query = "SELECT * \
            FROM dados_telefonia "
    fatos_telefonia = ps.sqldf(query)
    print(fatos_telefonia.columns)
    '''

def gen_dim_tempo_telefonia():
    dados_telefonia = pd.read_csv("Acessos_Telefonia_Movel.csv", index_col=0)
    out_path = 'telefonia/'
    
    ### dim_tempo ###
    query = "SELECT Ano, Mês\
            FROM dados_telefonia"
    dim_tempo = ps.sqldf(query)

    dim_tempo.drop_duplicates(inplace=True)
    
    semestre_array = []
    for mes in dim_tempo['Mês']:
        if (mes <= 6):
            semestre_array.append(1)
        else:
            semestre_array.append(2)
    dim_tempo.insert(2, "Semestre", semestre_array)

    dim_tempo.reset_index(drop=True, inplace=True)

    PK = []
    for i in range(1, len(dim_tempo)+1):
        PK.append(i)
    dim_tempo.insert(3, "PK", PK)

    print(dim_tempo)
    dim_tempo.to_csv(out_path+'dim_tempo.csv')

def gen_dim_tecnologia():
    dados_telefonia = pd.read_csv("Acessos_Telefonia_Movel.csv", index_col=0)
    out_path = 'telefonia/'

    ### dim_tecnologia ###
    query = "SELECT Tecnologia AS Nome, Tecnologia_Geração as Geração \
            FROM dados_telefonia \
            ORDER BY Tecnologia_Geração"
    dim_tecnologia = ps.sqldf(query)
    dim_tecnologia.drop_duplicates(inplace=True)
    dim_tecnologia.reset_index(drop=True, inplace=True)

    PK = []
    for i in range(1, len(dim_tecnologia)+1):
        PK.append(i)
    dim_tecnologia.insert(2, "PK", PK)

    print(dim_tecnologia)
    dim_tecnologia.to_csv(out_path+"dim_tecnologia.csv")

def gen_dim_local():
    dados_telefonia = pd.read_csv("Acessos_Telefonia_Movel.csv", index_col=0)
    out_path = 'telefonia/'

    ### dim_local ###
    query = "SELECT UF, Código_Nacional \
            FROM dados_telefonia \
            ORDER BY UF"
    dim_local = ps.sqldf(query)
    dim_local.drop_duplicates(inplace=True)
    dim_local.reset_index(drop=True, inplace=True)

    regiao_array = []
    for estado in dim_local['UF']:
        if (estado in {'SP', 'MG', 'ES', 'RJ'}):
            regiao_array.append('SUDESTE')
        elif (estado in {'PN', 'SC', 'RS'}):
            regiao_array.append('SUL')
        elif (estado in {'AC', 'AM', 'RN', 'RR', 'AM', 'PA', 'TO'}):
            regiao_array.append('NORTE')
        elif (estado in {'MT', 'MS', 'GO', 'FR'}):
            regiao_array.append('CENTRO-OESTE')
        else:
            regiao_array.append('NORDESTE')
    dim_local.insert(2, "Região", regiao_array)

    PK = []
    for i in range(1, len(dim_local)+1):
        PK.append(i)
    dim_local.insert(2, "PK", PK)

    print(dim_local)
    dim_local.to_csv(out_path+"dim_local.csv")

def preparar_dimensoes_telefonia():
    dados_telefonia = pd.read_csv("Acessos_Telefonia_Movel.csv", index_col=0)
    out_path = 'telefonia/'

    gen_dim_tempo_telefonia()
    gen_dim_tecnologia()
    gen_dim_local()

def gen_apps_csv():
    path = 'data/app_magic/data/ranking/'
    out_path = 'data/app_magic/data/csv/'

    starting_year = 2015
    last_year = 2022
    last_month = 9

    for year in range(starting_year, last_year):
        for month in range(1, 13):
            with open(path+str(year)+'-'+str(month)+'.json') as f:
                data = json.load(f)
                df = pd.json_normalize(data, record_path=['data'], )
                df.to_csv(out_path+str(year)+'-'+str(month)+'.csv')

    for month in range(1, last_month+1):
            with open(path+str(last_year)+'-'+str(month)+'.json') as f:
                data = json.load(f)
                df = pd.json_normalize(data, record_path=['data'], )
                df.to_csv(out_path+str(last_year)+'-'+str(month)+'.csv')

    print(df)
    print(df.info())

def add_month_year_to_df(df, year, month):
    year_column = []
    month_column = []
    for i in range(1, len(df)+1):
        year_column.append(year)
        month_column.append(month)
    df.insert(0, "year", year_column)
    df.insert(0, "month", month_column)

def gen_apps_tratados():
    path = 'data/app_magic/data/csv/'
    out_path = 'app_magic_tratados/'

    starting_year = 2015
    last_year = 2022
    last_month = 9

    columns_new_name = ['top_featuring', 'top_free_top_free', 'top_free_diff', 
            'top_free_application_united_application_id',
            'top_free_application_name', 'top_free_application_icon_url',
            'top_free_application_stores', 'top_free_application_categories',
            'top_free_application_store_application_ids',
            'top_free_application_store_ids', 'top_free_application_publisherIds',
            'top_free_application_tags', 'top_free_application_publisher_id',
            'top_free_application_publisher_name', 'top_grossing_top_grossing',
            'top_grossing_diff', 'top_grossing_application_united_application_id',
            'top_grossing_application_name', 'top_grossing_application_icon_url',
            'top_grossing_application_stores',
            'top_grossing_application_categories',
            'top_grossing_application_store_application_ids',
            'top_grossing_application_store_ids',
            'top_grossing_application_publisherIds',
            'top_grossing_application_tags',
            'top_grossing_application_publisher_id',
            'top_grossing_application_publisher_name']

    for year in range(starting_year, 2018):
        for month in range(1, 13):
            df = pd.read_csv(path+str(year)+'-'+str(month)+'.csv', index_col=0)
            df.columns = columns_new_name
            query = "SELECT top_free_top_free, top_free_application_united_application_id, top_free_application_name, top_free_application_icon_url, top_free_application_tags \
            FROM df "
            df = ps.sqldf(query)
            add_month_year_to_df(df, year, month)
            df.to_csv(out_path+'free_'+str(year)+'-'+str(month)+'.csv')

            df = pd.read_csv(path+str(year)+'-'+str(month)+'.csv', index_col=0)
            df.columns = columns_new_name
            query = "SELECT top_grossing_top_grossing, top_grossing_application_united_application_id, top_grossing_application_name, top_grossing_application_icon_url, top_grossing_application_tags \
            FROM df "
            df = ps.sqldf(query)
            add_month_year_to_df(df, year, month)
            df.to_csv(out_path+'grossing_'+str(year)+'-'+str(month)+'.csv')
        print(f"{year} done")

    for year in [2018]:
        for month in range(1, 8):
            df = pd.read_csv(path+str(year)+'-'+str(month)+'.csv', index_col=0)
            df.columns = columns_new_name
            query = "SELECT top_free_top_free, top_free_application_united_application_id, top_free_application_name, top_free_application_icon_url, top_free_application_tags \
            FROM df "
            df = ps.sqldf(query)
            add_month_year_to_df(df, year, month)
            df.to_csv(out_path+'free_'+str(year)+'-'+str(month)+'.csv')

            df = pd.read_csv(path+str(year)+'-'+str(month)+'.csv', index_col=0)
            df.columns = columns_new_name
            query = "SELECT top_grossing_top_grossing, top_grossing_application_united_application_id, top_grossing_application_name, top_grossing_application_icon_url, top_grossing_application_tags \
            FROM df "
            df = ps.sqldf(query)
            add_month_year_to_df(df, year, month)
            df.to_csv(out_path+'grossing_'+str(year)+'-'+str(month)+'.csv')
    print(f"{year} until month 7 done")

    columns_new_name = ['top_free_top_free', 'top_free_diff',
       'top_free_application_united_application_id',
       'top_free_application_name', 'top_free_application_icon_url',
       'top_free_application_stores', 'top_free_application_categories',
       'top_free_application_store_application_ids',
       'top_free_application_store_ids', 'top_free_application_publisherIds',
       'top_free_application_tags', 'top_free_application_publisher_id',
       'top_free_application_publisher_name', 'top_grossing_top_grossing',
       'top_grossing_diff', 'top_grossing_application_united_application_id',
       'top_grossing_application_name', 'top_grossing_application_icon_url',
       'top_grossing_application_stores',
       'top_grossing_application_categories',
       'top_grossing_application_store_application_ids',
       'top_grossing_application_store_ids',
       'top_grossing_application_publisherIds',
       'top_grossing_application_tags',
       'top_grossing_application_publisher_id',
       'top_grossing_application_publisher_name',
       'top_featuring_top_featuring', 'top_featuring_featuring',
       'top_featuring_diff', 'top_featuring_application_united_application_id',
       'top_featuring_application_name', 'top_featuring_application_icon_url',
       'top_featuring_application_stores',
       'top_featuring_application_categories',
       'top_featuring_application_store_application_ids',
       'top_featuring_application_store_ids',
       'top_featuring_application_publisherIds',
       'top_featuring_application_tags',
       'top_featuring_application_publisher_id',
       'top_featuring_application_publisher.name']

    for year in [2018]:
        for month in range(8, 13):
            df = pd.read_csv(path+str(year)+'-'+str(month)+'.csv', index_col=0)
            df.columns = columns_new_name
            query = "SELECT top_free_top_free, top_free_application_united_application_id, top_free_application_name, top_free_application_icon_url, top_free_application_tags \
            FROM df "
            df = ps.sqldf(query)
            add_month_year_to_df(df, year, month)
            df.to_csv(out_path+'free_'+str(year)+'-'+str(month)+'.csv')

            df = pd.read_csv(path+str(year)+'-'+str(month)+'.csv', index_col=0)
            df.columns = columns_new_name
            query = "SELECT top_grossing_top_grossing, top_grossing_application_united_application_id, top_grossing_application_name, top_grossing_application_icon_url, top_grossing_application_tags \
            FROM df "
            df = ps.sqldf(query)
            add_month_year_to_df(df, year, month)
            df.to_csv(out_path+'grossing_'+str(year)+'-'+str(month)+'.csv')
    print(f"{year} done")

    for year in range(2019, last_year):
        for month in range(1, 13):
            df = pd.read_csv(path+str(year)+'-'+str(month)+'.csv', index_col=0)
            df.columns = columns_new_name
            query = "SELECT top_free_top_free, top_free_application_united_application_id, top_free_application_name, top_free_application_icon_url, top_free_application_tags \
            FROM df "
            df = ps.sqldf(query)
            add_month_year_to_df(df, year, month)
            df.to_csv(out_path+'free_'+str(year)+'-'+str(month)+'.csv')

            df = pd.read_csv(path+str(year)+'-'+str(month)+'.csv', index_col=0)
            df.columns = columns_new_name
            query = "SELECT top_grossing_top_grossing, top_grossing_application_united_application_id, top_grossing_application_name, top_grossing_application_icon_url, top_grossing_application_tags \
            FROM df "
            df = ps.sqldf(query)
            add_month_year_to_df(df, year, month)
            df.to_csv(out_path+'grossing_'+str(year)+'-'+str(month)+'.csv')
        print(f"{year} done")
    
    year = 2022
    try:
        for month in range(1, 13):
            df = pd.read_csv(path+str(year)+'-'+str(month)+'.csv', index_col=0)
            df.columns = columns_new_name
            query = "SELECT top_free_top_free, top_free_application_united_application_id, top_free_application_name, top_free_application_icon_url, top_free_application_tags \
            FROM df "
            df = ps.sqldf(query)
            add_month_year_to_df(df, year, month)
            df.to_csv(out_path+'free_'+str(year)+'-'+str(month)+'.csv')

            df = pd.read_csv(path+str(year)+'-'+str(month)+'.csv', index_col=0)
            df.columns = columns_new_name
            query = "SELECT top_grossing_top_grossing, top_grossing_application_united_application_id, top_grossing_application_name, top_grossing_application_icon_url, top_grossing_application_tags \
            FROM df "
            df = ps.sqldf(query)
            add_month_year_to_df(df, year, month)
            df.to_csv(out_path+'grossing_'+str(year)+'-'+str(month)+'.csv')
    except:
        print(f"last mounth: {month}")

    free_df = pd.read_csv(out_path+'free_2015-1.csv', index_col=0)
    free_df = pd.DataFrame(columns=free_df.columns)
    #
    grossing_df = pd.read_csv(out_path+'grossing_2015-1.csv', index_col=0)
    grossing_df = pd.DataFrame(columns=grossing_df.columns)
    #
    for year in range(starting_year, last_year+1):
        for month in range(1, 13):
            try:
                df = pd.read_csv(out_path+'free_'+str(year)+'-'+str(month)+'.csv', index_col=0)
                free_df = free_df.append(df)
                #
                df = pd.read_csv(out_path+'grossing_'+str(year)+'-'+str(month)+'.csv', index_col=0)
                grossing_df = grossing_df.append(df)
            except:
                pass
    
    free_df.to_csv('magic_apps/free.csv')
    grossing_df.to_csv('magic_apps/grossing.csv')

def gen_dim_tempo_app_magic():
    input = pd.read_csv("magic_apps/free.csv", index_col=0)
    out_path = 'magic_apps/'
    
    ### dim_tempo ###
    query = "SELECT year AS Ano, Month AS Mês\
            FROM input"
    dim_tempo = ps.sqldf(query)

    dim_tempo.drop_duplicates(inplace=True)
    
    semestre_array = []
    for mes in dim_tempo['Mês']:
        if (mes <= 6):
            semestre_array.append(1)
        else:
            semestre_array.append(2)
    dim_tempo.insert(2, "Semestre", semestre_array)

    dim_tempo.reset_index(drop=True, inplace=True)

    PK = []
    for i in range(1, len(dim_tempo)+1):
        PK.append(i)
    dim_tempo.insert(3, "PK", PK)

    print(dim_tempo)
    dim_tempo.to_csv(out_path+'dim_tempo.csv')

def gen_dim_aplicativo():
    free_apps = pd.read_csv("magic_apps/free.csv", index_col=0)
    grossing_apps = pd.read_csv("magic_apps/grossing.csv", index_col=0)
    out_path = 'magic_apps/'
    
    query = "SELECT top_free_application_united_application_id AS PK, top_free_application_name AS Nome, top_free_application_icon_url as URL\
            FROM free_apps"
    dim_aplicativo_free = ps.sqldf(query)
    dim_aplicativo_free.drop_duplicates(inplace=True)

    query = "SELECT top_grossing_application_united_application_id AS PK, top_grossing_application_name AS Nome, top_grossing_application_icon_url as URL\
            FROM grossing_apps"
    dim_aplicativo_grossing = ps.sqldf(query)
    dim_aplicativo_grossing.drop_duplicates(inplace=True)
    
    dim_aplicativo = dim_aplicativo_free.append(dim_aplicativo_grossing)
    dim_aplicativo.drop_duplicates(inplace=True)
    
    dim_aplicativo.reset_index(drop=True, inplace=True)

    print(dim_aplicativo)
    dim_aplicativo.to_csv(out_path+'dim_aplicativo.csv')

def intersecao():
    free_apps = pd.read_csv("magic_apps/free.csv", index_col=0)
    grossing_apps = pd.read_csv("magic_apps/grossing.csv", index_col=0)
    out_path = 'magic_apps/'
    
    # Intersecção para verificar se há aplicativos em ambas categorias #
    query = "SELECT top_grossing_application_name AS Nome\
            FROM grossing_apps, free_apps\
            WHERE free_apps.top_free_application_name = grossing_apps.top_grossing_application_name"
    intersection = ps.sqldf(query)
    intersection.drop_duplicates(inplace=True)
    intersection.reset_index(drop=True, inplace=True)
    #print(intersection)
    intersection.to_csv(out_path+'intersection.csv')
    ####################################################################

def gen_dim_categoria():
    free_apps = pd.read_csv("magic_apps/free.csv", index_col=0)
    grossing_apps = pd.read_csv("magic_apps/grossing.csv", index_col=0)
    out_path = 'magic_apps/'
    
    query = "SELECT top_free_application_tags\
            FROM free_apps"
    categorias_free = ps.sqldf(query)
    categorias_free.drop_duplicates(inplace=True)
    categorias_free.reset_index(drop=True, inplace=True)
    
    import ast
    categorias_set = set()

    for i in range(0, len(categorias_free)):
        categorias_free.iloc[i]['top_free_application_tags'] = ast.literal_eval(categorias_free.iloc[i]['top_free_application_tags'])
    for i in range(0, len(categorias_free)):
        for j in range (0, len(categorias_free.iloc[i]['top_free_application_tags'])):
            categorias_set.add(categorias_free.iloc[i]['top_free_application_tags'][j].get('name'))
    
    #print(categorias_free)

    query = "SELECT top_grossing_application_tags\
            FROM grossing_apps"
    categorias_grossing = ps.sqldf(query)
    categorias_grossing.drop_duplicates(inplace=True)
    categorias_grossing.reset_index(drop=True, inplace=True)

    for i in range(0, len(categorias_grossing)):
        categorias_grossing.iloc[i]['top_grossing_application_tags'] = ast.literal_eval(categorias_grossing.iloc[i]['top_grossing_application_tags'])
    for i in range(0, len(categorias_grossing)):
        for j in range (0, len(categorias_grossing.iloc[i]['top_grossing_application_tags'])):
            categorias_set.add(categorias_grossing.iloc[i]['top_grossing_application_tags'][j].get('name'))

    #print(categorias_grossing)
    #print(categorias_set)

    categorias_list = list(categorias_set)

    data = list()
    for i in range(0, len(categorias_list)):
        data.append([categorias_list[i], i+1])
    dim_categoria = pd.DataFrame(data, columns=['Categoria', 'PK'])
    print(dim_categoria)
    dim_categoria.to_csv(out_path+'dim_categoria.csv')

def preparar_fatos_telefonia():
    path = 'telefonia/'
    dados_telefonia = pd.read_csv("Acessos_Telefonia_Movel.csv", index_col=0)
    dim_tempo = pd.read_csv(path+"dim_tempo.csv", index_col=0)
    dim_tecnologia = pd.read_csv(path+"dim_tecnologia.csv", index_col=0)
    dim_local = pd.read_csv(path+"dim_local.csv", index_col=0)

    query = "SELECT PK as id_tempo ,UF, Código_Nacional, Tecnologia, Tecnologia_Geração, Acessos\
            FROM dados_telefonia, dim_tempo\
            WHERE dados_telefonia.Ano = dim_tempo.Ano AND dados_telefonia.'Mês' = dim_tempo.'Mês'"
    fatos_telefonia = ps.sqldf(query)

    query = "SELECT id_tempo ,PK as id_local, Tecnologia, Tecnologia_Geração, Acessos\
            FROM fatos_telefonia, dim_local\
            WHERE fatos_telefonia.'Código_Nacional' = dim_local.'Código_Nacional'"
    fatos_telefonia = ps.sqldf(query)

    query = "SELECT id_tempo ,id_local, PK as id_tecnologia, Acessos\
            FROM fatos_telefonia, dim_tecnologia\
            WHERE fatos_telefonia.Tecnologia = dim_tecnologia.Nome AND fatos_telefonia.'Tecnologia_Geração' = dim_tecnologia.'Geração'"
    fatos_telefonia = ps.sqldf(query)

    PK = []
    for i in range(1, len(fatos_telefonia)+1):
        PK.append(i)
    fatos_telefonia.insert(0, "PK", PK)

    print(fatos_telefonia)

    fatos_telefonia.to_csv(path+"fatos_telefonia.csv")

def gen_fatos_apps():
    free_apps = pd.read_csv("magic_apps/free.csv", index_col=0)
    grossing_apps = pd.read_csv("magic_apps/grossing.csv", index_col=0)
    
    path = 'magic_apps/'
    dim_tempo = pd.read_csv(path+"dim_tempo.csv", index_col=0)
    dim_categoria = pd.read_csv(path+"dim_categoria.csv", index_col=0)
    dim_aplicativo = pd.read_csv(path+"dim_aplicativo.csv", index_col=0)

    Gratuito = []
    for i in range(0, len(free_apps)):
        Gratuito.append(True)
    free_apps.insert(0, "Gratuito", Gratuito)

    Gratuito = []
    for i in range(0, len(grossing_apps)):
        Gratuito.append(False)
    grossing_apps.insert(0, "Gratuito", Gratuito)

    free_apps.rename(columns={'top_free_top_free': 'Ranking', 'top_free_application_name': 'Nome', 'top_free_application_icon_url':'URL', 'top_free_application_tags' : 'Categorias' }, inplace=True)
    grossing_apps.rename(columns={'top_grossing_top_grossing': 'Ranking', 'top_grossing_application_name': 'Nome', 'top_grossing_application_icon_url':'URL', 'top_grossing_application_tags' : 'Categorias' }, inplace=True)

    query = "SELECT month, year, Ranking, Nome, URL, Categorias\
            FROM free_apps"
    free_apps = ps.sqldf(query)
    query = "SELECT month, year, Ranking, Nome, URL, Categorias\
            FROM grossing_apps"
    grossing_apps = ps.sqldf(query)

    fatos_apps = free_apps.append(grossing_apps)
    fatos_apps.drop_duplicates(inplace=True)
    fatos_apps.reset_index(inplace=True)

    '''
    query = "SELECT id_tempo ,PK as id_local, Tecnologia, Tecnologia_Geração, Acessos\
            FROM fatos_telefonia, dim_local\
            WHERE fatos_telefonia.'Código_Nacional' = dim_local.'Código_Nacional'"
    fatos_apps = ps.sqldf(query)

    query = "SELECT id_tempo ,id_local, PK as id_tecnologia, Acessos\
            FROM fatos_telefonia, dim_tecnologia\
            WHERE fatos_telefonia.Tecnologia = dim_tecnologia.Nome AND fatos_telefonia.'Tecnologia_Geração' = dim_tecnologia.'Geração'"
    fatos_apps = ps.sqldf(query)
    '''
    ##

    PK = []
    for i in range(1, len(fatos_apps)+1):
        PK.append(i)
    fatos_apps.insert(0, "PK", PK)

    print(fatos_apps)

    fatos_apps.to_csv(path+"fatos_apps.csv")

def telefonia_consultas():
    path = 'telefonia/'
    fatos_telefonia = pd.read_csv(path+"fatos_telefonia.csv", index_col=0)
    dim_tempo = pd.read_csv(path+"dim_tempo.csv", index_col=0)
    dim_tecnologia = pd.read_csv(path+"dim_tecnologia.csv", index_col=0)
    dim_local = pd.read_csv(path+"dim_local.csv", index_col=0)
    
    '''Roll-up dos fatos de telefonia (densidade de acesso) agrupando em anos,
     para visualizar a evolução do acesso à internet móvel ao longo do tempo medido. 
     (Busca responder-se: Em qual ano houve um maior crescimento de acesso à rede móvel?)'''
    query = "SELECT Ano, SUM(Acessos) \
            FROM fatos_telefonia, dim_tempo \
            WHERE fatos_telefonia.id_tempo = dim_tempo.PK \
            GROUP BY Ano"
    rollup = ps.sqldf(query)
    print(rollup)
    rollup.to_csv("rollup.csv")

    '''Pivot, substituindo o tempo por Geração da Tecnologia 
    (Busca responder-se: O crescimento do acesso em relação à geração coincide com o crescimento
     em relação ao tempo?)'''
    query = "SELECT Geração, SUM(Acessos) \
            FROM fatos_telefonia, dim_tecnologia \
            WHERE fatos_telefonia.id_tecnologia = dim_tecnologia.PK \
            GROUP BY Geração"
    pivot = ps.sqldf(query)
    print(pivot)
    pivot.to_csv("pivot.csv")

    '''Drill-down da Geração da Tecnologia para a Tecnologia Específica para visualizar o impacto
    da mudança de tecnologia dentro da mesma geração
     (Busca responder-se: Qual geração de tecnologia mais contribuiu para o acesso à rede móvel?
      E qual tecnologia específica?)'''
    query = "SELECT Nome, Geração, SUM(Acessos) \
            FROM fatos_telefonia, dim_tecnologia \
            WHERE fatos_telefonia.id_tecnologia = dim_tecnologia.PK \
            GROUP BY Nome"
    drill_down = ps.sqldf(query)
    print(drill_down)
    drill_down.to_csv("drill_down.csv")

    '''Slice da densidade de acesso da região sudeste, composto pelas Unidades Federativas
     de São Paulo, Rio de janeiro, Espírito Santo e Minas Gerais, para encontrar as peculiaridades
      desta região específica
       (Busca responder-se: Como a região sudeste se compara com a média observada?)'''
    query = "SELECT UF, SUM(Acessos) \
            FROM fatos_telefonia, dim_local \
            WHERE fatos_telefonia.id_local = dim_local.PK AND dim_local.'Região' = 'SUDESTE' \
            GROUP BY UF"
    slice = ps.sqldf(query)
    print(slice)
    slice.to_csv("slice.csv")

def telefonia_consultas_plot():
    rollup = pd.read_csv('rollup.csv', index_col=0)
    pivot = pd.read_csv('pivot.csv', index_col=0)
    drill_down = pd.read_csv('drill_down.csv', index_col=0)
    slice = pd.read_csv('slice.csv', index_col=0)

    rollup.plot(kind='line',x='Ano',y='SUM(Acessos)',color='red')
    pivot.plot(kind='bar',x='Geração',y='SUM(Acessos)',color='red')
    drill_down.plot(kind='bar',x='Nome',y='SUM(Acessos)',color='red')
    slice.plot(kind='bar',x='UF',y='SUM(Acessos)',color='red')

    plt.show()

def consulta_drill_across():
    telefonia_path = 'telefonia/'
    fatos_telefonia = pd.read_csv(telefonia_path+"fatos_telefonia.csv", index_col=0)
    dim_tempo = pd.read_csv(telefonia_path+"dim_tempo.csv", index_col=0)
    dim_tecnologia = pd.read_csv(telefonia_path+"dim_tecnologia.csv", index_col=0)
    dim_local = pd.read_csv(telefonia_path+"dim_local.csv", index_col=0)
    
    apps_path = 'app_magic_tratados/'

    #
    '''Drill-Across, traçando a dimensão tempo no eixo X.
     No eixo Y, o traço da densidade de acesso (em 100 acessos por mil habitantes);
      as categorias nos primeiros 5 colocados (rede social, jogos) e a proporção de aplicativos
      gratuitos nos primeiros 10 colocados do ranking de popularidade.
    #(Busca responder-se: Há correlação entre a popularidade de certos aplicativos com o
     crescimento de densidade de acesso? Os aplicativos dominantes são sempre gratuitos?
      Quais aplicativos não gratuitos dominam para cada grau de acesso?)'''
    query = ""
    #drill_across = ps.sqldf(query)
    #print(drill_across)
    #drill_across.to_csv("drill_across.csv")

if __name__ == '__main__':
    #gen_telefonia()
    #preparar_dimensoes_telefonia()
    #preparar_fatos_telefonia()
    #telefonia_consultas()
    telefonia_consultas_plot()


    #gen_apps_csv()
    #gen_apps_tratados()
    #gen_dim_tempo_app_magic()
    #gen_dim_aplicativo()
    #gen_dim_categoria()
    gen_fatos_apps()
    consulta_drill_across()
